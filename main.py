import json
import pickle
import random as r
from collections import namedtuple
from itertools import chain

import arrow


class BaseItem:
    def __init__(self):
        pass


class Mod:
    def __init__(self, mod_id: str):
        self.mod_id = mod_id
        self.properties = {}
        self.ilvl = None
        self.name = None
        self.weight = None
        self.adds_tags = None
        self.group = None
        self.mod_type = None
        self.stats = None
        self.prefix = None
        self.suffix = None
        self.domain = None

    def parse_properties(self) -> bool:
        self.mod_type = self.properties['generation_type']

        if self.mod_type == 'prefix':
            self.prefix = True
        elif self.mod_type == 'suffix':
            self.suffix = True
        else:
            return False

        fossil_mod_list = []
        influences = {}
        if our_options.fossils:
            # make a list of fossil mods that can spawn
            temp_ = [v['fossil_mods'] for k, v in all_fossils.items() if k in our_options.fossils]
            fossil_mod_list = list(chain.from_iterable(temp_))

            # populate influences
            # they might look like this:
            # {'gem_level': 10.0, 'bleed': 0.0, 'poison': 0.0, 'fire': 5.0}
            for k, v in all_fossils.items():
                if k in our_options.fossils:
                    if 'influence_tags' in v:
                        for infl in v['influence_tags']:
                            infl_type = infl['tag']
                            infl_value = infl['influence']
                            if infl_type in influences:
                                influences[infl_type] *= infl_value * 0.01
                            else:
                                influences[infl_type] = infl_value * 0.01

        self.domain = self.properties['domain']

        if self.domain == 'delve':
            if self.mod_id not in fossil_mod_list:
                return False
        elif self.domain != 'item':
            return False

        self.ilvl = self.properties['required_level']
        if self.ilvl > our_options.ilvl:
            return False

        tags = base_item['tags']
        item_class = base_item['item_class']
        shaper_tag = item_classes[item_class]['shaper_tag']
        elder_tag = item_classes[item_class]['elder_tag']
        if our_options.item_type == 'shaper':
            tags.insert(0, shaper_tag)
        elif our_options.item_type == 'elder':
            tags.insert(0, elder_tag)

        for entry in self.properties['spawn_weights']:
            if entry['tag'] in tags:
                self.weight = entry["weight"]
                break

        self.adds_tags = self.properties['adds_tags']

        if self.weight == 0:
            return False

        if our_options.fossils:
            for k, v in influences.items():
                if k in self.adds_tags:
                    self.weight = int(self.weight * v)

        if self.weight == 0:
            return False

        self.name = self.properties['name']
        self.group = self.properties['group']
        self.stats = self.properties['stats']

        return True


class AllMods:
    def __init__(self, filename: str = None):
        self.mods = {}
        self.filename = filename

        if filename:
            self.import_from_file()

    def import_from_file(self) -> None:
        for k, v in load_json(self.filename).items():
            mod_candidate = Mod(k)
            for k2, v2 in v.items():
                mod_candidate.properties[k2] = v2
            if mod_candidate.parse_properties():
                self.mods[k] = mod_candidate

    @staticmethod
    def round_probability(probability: float) -> float:
        if probability > 10:
            n = 1
        elif probability > 1:
            n = 2
        elif probability > 0.1:
            n = 3
        elif probability > 0.01:
            n = 3
        else:
            n = 4
        return round(probability, n)

    def show_all_mods_in_db(self, filename: str) -> None:
        separ = '-----' * 30
        id_col = 'mod internal id'
        name_col = 'mod name'
        tag_col = 'fossil and meta tags'

        stats = collect_stats(self.mods)

        l1 = stats['longest_mod_id']
        l2 = stats['longest_mod_name']

        f = open(filename, 'w')
        print(f' #    type   {id_col:^{l1}} ilvl  {name_col:^{l2}} weight{tag_col:^32}\n{separ}',
              file=f)

        c = 0
        for k, v in self.mods.items():
            c += 1
            tags = ', '.join(v.adds_tags)
            print(f'{c:3}  {v.mod_type}  {k:{l1}}  {v.ilvl:2}  {v.name:{l2}}  {v.weight:5}  {tags}',
                  file=f)

        print(separ, file=f)

    # todo: make this fucking method shorter
    def show_mods(self, filename: str) -> None:
        separ = '-----' * 27
        separ2 = '-----' * 11
        id_column = 'mod internal id'
        name_column = 'mod name'
        chance_column = 'chance'
        chance_column_2 = 'chance within affix type'

        ilvl = our_options.ilvl
        fossils_used = our_options.fossils

        # filtered_mods, kill_counter = filter_mods(options)
        stats = collect_stats(all_mods.mods)

        t_prefix = stats['pref_weight']
        t_suffix = stats['suff_weight']
        total = stats['total_weight']
        l1 = stats['longest_mod_id']
        l2 = stats['longest_mod_name']
        add_counter = stats['add_counter']
        pref_amount = stats['pref_amount']
        suff_amount = stats['suff_amount']
        prefix_share = round(t_prefix / total * 100, 1)
        suffix_share = round(t_suffix / total * 100, 1)

        f = open(filename, 'w')

        print(f'List of all mods for ilvl {ilvl} {our_options.item_type} '
              f'{our_options.base_item_name}', file=f)
        if fossils_used:
            print(f'Fossils used: {", ".join(fossils_used)}', file=f)
        print(f'{separ}\n #   {id_column:^{l1}} ilvl  {name_column:^{l2}} weight  '
              f'{chance_column:^18} {chance_column_2:^18}\n{separ}', file=f)

        c = 0
        for k, v in all_mods.mods.items():
            prob2 = None
            c += 1
            prob = v.weight / total * 100
            prob = self.round_probability(prob)
            odds = round(total / v.weight)

            # to check probability within affix type (prefix/suffix) we need to use
            # the corresponding weight. this is a little trick to make it quicker
            needed_total = bool(v.prefix) * t_prefix + bool(v.suffix) * t_suffix
            prob2 = v.weight / needed_total * 100
            prob2 = self.round_probability(prob2)
            odds2 = round(needed_total / v.weight)

            prob_literal = f'{prob}%, 1 in {odds}'
            prob2_literal = f'{prob2}%, 1 in {odds2}'

            print(f'{c:3}  {k:{l1}}  {v.ilvl:2}  {v.name:{l2}}  {v.weight:5}    '
                  f'{prob_literal:20} {prob2_literal:20}', file=f)
        print(separ, file=f)

        print(f'{our_options.item_type} {our_options.base_item_name}, ilvl {ilvl}', file=f)
        print(f'Prefixes: {pref_amount}\nSuffixes: {suff_amount}', file=f)
        if fossils_used:
            print(f'{separ2}\nFossils used: {", ".join(fossils_used)}', file=f)
        if add_counter and add_counter > 0:
            print(f'Mods added with fossils: {add_counter}', file=f)
        # if kill_counter and kill_counter > 0:
        #     print(f'Mods removed with fossils: {kill_counter}', file=f)
        print(f'{separ2}\nTotal weight of prefixes: {t_prefix}', file=f)
        print(f'Total weight of suffixes: {t_suffix}', file=f)
        print(f'Total mod weight: {total}', file=f)
        print(f'Prefix share: {prefix_share}%', file=f)
        print(f'Suffix share: {suffix_share}%', file=f)
        f.close()


class Fossils:
    def __init__(self, filename: str):
        with open(filename) as f:
            self.all = json.load(f)


class ChosenItem:
    PROB_4MOD = 65
    PROB_5MOD = 25
    PROB_6MOD = 10

    def __init__(self):
        pass

    @staticmethod
    def remove_mods_of_one_group(item: AllMods, group_name: str) -> AllMods:
        to_kill = []
        for k, v in item.mods.items():
            if v.group == group_name:
                to_kill.append(k)

        for entry in to_kill:
            del item.mods[entry]

        return item

    def generate(self) -> list:
        # determine the amount of mods
        mod_amount = r.choices([4, 5, 6], [self.PROB_4MOD, self.PROB_5MOD, self.PROB_6MOD])[0]

        # make a copy of current item to work with it
        new_roll = AllMods()
        new_roll.mods = {k: v for k, v in all_mods.mods.items()}

        prefixes = 0
        suffixes = 0
        rolled_mods = []
        for i in range(mod_amount):
            prefix_pool = []
            prefix_weights = []
            suffix_pool = []
            suffix_weights = []
            total_pool = []
            total_weights = []

            # populate affix pools and weights
            for k, v in new_roll.mods.items():
                if v.prefix:
                    prefix_pool.append(k)
                    prefix_weights.append(v.weight)
                elif v.suffix:
                    suffix_pool.append(k)
                    suffix_weights.append(v.weight)

            # if we hit 3 prefixes we can't roll prefixes anymore, etc
            if prefixes == 3:
                total_pool = suffix_pool
                total_weights = suffix_weights
            elif suffixes == 3:
                total_pool = prefix_pool
                total_weights = prefix_weights
            else:
                total_pool = prefix_pool + suffix_pool
                total_weights = prefix_weights + suffix_weights

            # if there's nothing to pick from, we stop rolling prematurely
            if len(total_pool) == 0:
                return rolled_mods

            new_mod = r.choices(total_pool, total_weights)[0]

            if is_prefix(new_roll, new_mod):
                prefixes += 1
            else:
                suffixes += 1
            rolled_mods.append(new_mod)

            # remove the entire group of a mod we just rolled, we can't roll it anymore
            group = new_roll.mods[new_mod].group
            new_roll = self.remove_mods_of_one_group(new_roll, group)

        return rolled_mods


class Item:
    def __init__(self, mods: list, ilvl: int):
        self.mods = mods
        self.ilvl = ilvl

        self.prefixes = 0
        self.suffixes = 0
        self.parsed = {}

        self.parse_item()
        self.open_prefixes = 3 - self.prefixes
        self.open_suffixes = 3 - self.suffixes

    def parse_item(self) -> None:
        for each_mod in self.mods:
            this_mod = find_mod_by_id(each_mod)
            if this_mod.prefix:
                self.prefixes += 1
            else:
                self.suffixes += 1
            self.parsed[each_mod] = this_mod

    def check_combo(self, combo_set: tuple, open_pref: int = None,
                    open_suff: int = None) -> bool:
        if open_pref is not None and self.open_prefixes != open_pref:
            return False
        if open_suff is not None and self.open_suffixes != open_suff:
            return False
        for combo in combo_set:
            if set(combo).issubset(self.parsed):
                return True
        return False

    @staticmethod
    def generate_rare_name() -> str:
        with open('data/rare_names.json') as f:
            contents = json.load(f)
            pref = r.choice(contents['prefixes'])
            suff = r.choice(contents['suffixes'])

        return f'{pref} {suff}'

    def print_item(self, f: object = None) -> None:
        rare_name = self.generate_rare_name()
        print(f'{rare_name}', file=f)
        print(f'ilvl {self.ilvl}', file=f)
        pref_end = '' if self.prefixes == 1 else 'es'
        suff_end = '' if self.suffixes == 1 else 'es'
        print(f'{self.prefixes} prefix{pref_end}, {self.suffixes} suffix{suff_end}', file=f)
        # sorting the dictionary by value so prefixes come first
        for k, v in sorted(self.parsed.items(), key=lambda x: x[1].mod_type):
            print(f'{v.mod_type}  {k}\t{v.name}', file=f)
        print('', file=f)


class ItemPickle:
    def __init__(self):
        self.ilvl = our_options.ilvl
        self.times = our_options.times
        fossils = our_options.fossils
        i_type = our_options.item_type
        i_name = our_options.base_item_name

        self.name = construct_pickle_name(self.ilvl, i_type, i_name, self.times, fossils)

        f = open(self.name, 'rb')
        self.all = pickle.load(f)

    def check_combo(self, combo: dict, save_to_file: bool = False) -> None:
        txt_name = 'selected_items.txt'
        combo_mods = combo['mods']
        open_pref = combo['open_pref']
        open_suff = combo['open_suff']

        combo_counter = 0
        with open(txt_name, 'w') as file:
            for one in self.all:
                a_item = Item(one, self.ilvl)
                if a_item.check_combo(combo_mods, open_pref=open_pref, open_suff=open_suff):
                    combo_counter += 1
                    if save_to_file:
                        a_item.print_item(file)

        print(f'{our_options.item_type} {our_options.base_item_name}, ilvl {our_options.ilvl}')
        if our_options.fossils:
            print(f'Fossils used: {", ".join(our_options.fossils)}')
        print(f'Items scanned: {self.times}\n')
        print('Mod combo(s) to find:')
        for i, one_combo in enumerate(combo_mods):
            temp_ = [v.name for k, v in all_mods.mods.items() if k in one_combo]
            combo_name = ' + '.join(temp_)
            print(f"{i+1}) {' + '.join(one_combo)}")
            print(f'   {combo_name}')
        if open_pref:
            print(f'Open prefixes: {open_pref}.')
        if open_suff:
            print(f'Open suffixes: {open_suff}.')
        print(f'\nTotal found: {combo_counter}.')
        if combo_counter > 0:
            print(f'Odds: 1 in {round(self.times / combo_counter)}.')

    def save_all_items(self, txt_name: str) -> None:
        with open(txt_name, 'w') as file:
            for one in self.all:
                a_item = Item(one, self.ilvl)
                a_item.print_item(file)

    def how_many_mods(self) -> None:
        total_amount_of_prefixes = 0
        total_amount_of_suffixes = 0

        for one in self.all:
            a_item = Item(one, self.ilvl)
            total_amount_of_prefixes += a_item.prefixes
            total_amount_of_suffixes += a_item.suffixes

        print(f'Total prefixes: {total_amount_of_prefixes}. '
              f'Total suffixes: {total_amount_of_suffixes}.')


def timeit(f):
    """ Timing decorator.  """
    def timed(*args, **kw):
        ts = arrow.now()
        result = f(*args, **kw)
        te = arrow.now()
        print(f'Finished in {(te - ts).total_seconds():.2f}s')
        print('-' * 19)
        return result
    return timed


def load_json(filename: str) -> dict:
    with open(filename) as f:
        data = json.load(f)
    return data


def is_prefix(item: AllMods, mod_name: str) -> bool:
    if item.mods[mod_name].prefix:
        return True
    else:
        return False


def construct_pickle_name(ilvl: int, i_type: str, i_name: str, times: int, fossils: list) -> str:
    foss_name = ''
    if fossils:
        foss_name = '_' + '_'.join(fossils)
    name = f'pickles/ilvl_{ilvl}_{i_type}_{i_name}_{times}_rolls' + foss_name + '.pkl'

    return name


@timeit
def roll_item(to_cache: bool = True) -> None:
    ilvl = our_options.ilvl
    fossils = our_options.fossils
    i_type = our_options.item_type
    i_name = our_options.base_item_name
    times = our_options.times

    show_progress = True
    n = 5000

    new_item = ChosenItem()
    all_items = []
    
    for roll in range(times):
        if show_progress:
            if (roll+1) % n == 0:
                print(f'roll {roll+1}')
        new_mods = new_item.generate()
        all_items.append(new_mods)

    if to_cache:
        pkl = construct_pickle_name(ilvl, i_type, i_name, times, fossils)
        pickle.dump(all_items, open(pkl, 'wb'))


def collect_stats(mods: dict) -> dict:
    pref_weight = 0
    suff_weight = 0
    longest_mod_id = 0
    longest_mod_name = 0
    add_counter = 0
    pref_amount = 0
    suff_amount = 0

    for k, v in mods.items():
        if v.prefix:
            pref_weight += v.weight
            pref_amount += 1
        else:
            suff_weight += v.weight
            suff_amount += 1
        if len(k) > longest_mod_id:
            longest_mod_id = len(k)
        if len(v.name) > longest_mod_name:
            longest_mod_name = len(v.name)
        if v.domain == 'delve':
            add_counter += 1

    return {
        'pref_weight': pref_weight,
        'suff_weight': suff_weight,
        'total_weight': pref_weight + suff_weight,
        'longest_mod_id': longest_mod_id,
        'longest_mod_name': longest_mod_name,
        'add_counter': add_counter,
        'pref_amount': pref_amount,
        'suff_amount': suff_amount
    }


def find_mod_by_id(mod_id: str) -> Mod:
    return all_mods.mods[mod_id]


def find_base_item_by_name(name):
    for k, v in base_items.items():
        if v['name'] == name:
            return v


def construct_options_from_pkl_name(name):
    first_part, second_part = name.strip('.pkl').split('rolls')
    split_first = first_part.split('_')
    ilvl_ = split_first[1]
    type_ = split_first[2]
    name_ = split_first[3]
    times_ = split_first[4]

    fossils_ = []
    for x in second_part.split('_'):
        if x:
            fossils_.append(x)

    return ItemOptions(base_item_name=name_,
                       item_type=type_,
                       ilvl=int(ilvl_),
                       fossils=fossils_,
                       times=int(times_))


if __name__ == '__main__':
    global all_mods
    global all_fossils
    global base_items
    global item_classes
    global base_item
    global our_options

    ItemOptions = namedtuple('ItemOptions',
                             ['base_item_name', 'item_type', 'ilvl', 'fossils', 'times'])

    # =============================================================================================
    our_options = ItemOptions(base_item_name='Eclipse Staff',
                              item_type='normal',
                              ilvl=70,
                              fossils=('aetheric', 'metallic', 'corroded'),
                              times=100000)

    # naeym = 'ilvl_50_normal_Thicket Bow_1000000_rolls_faceted_prismatic_corroded_aetheric.pkl'
    # our_options = construct_options_from_pkl_name(naeym)
    # =============================================================================================

    base_items = load_json('data/base_items.json')
    base_item = find_base_item_by_name(our_options.base_item_name)
    item_classes = load_json('data/item_classes.json')
    all_fossils = load_json('data/fossils.json')
    all_mods = AllMods('data/mods.min.json')

    mods2 = (('CurseOnHitWarlordsMarkUber1_',
              'IncreasedLife7'),
             ('CurseOnHitWarlordsMarkUber1_',
              'IncreasedLife6'),
             ('CurseOnHitWarlordsMarkUber2',
              'IncreasedLife3'),
             ('CurseOnHitWarlordsMarkUber2',
              'IncreasedLife6'))

    our_combo = {
        'mods': (('DelveWeaponSocketedSpellsDamageFinal2h1',),),
        'open_pref': None,
        'open_suff': None,
    }

    # ========================
    roll_item()
    # ========================

    our_items = ItemPickle()
    our_items.check_combo(our_combo, save_to_file=True)
    # our_items.how_many_mods()
    # our_items.save_all_items('all_generated_items.txt')

    # all_mods.show_all_mods_in_db('all_mods_in_db.txt')
    # all_mods.show_mods('mod_report.txt')
